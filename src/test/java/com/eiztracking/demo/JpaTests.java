package com.eiztracking.demo;

import com.eiztracking.demo.entity.PO.Dict;
import com.eiztracking.demo.entity.PO.DictData;
import com.eiztracking.demo.service.DictDataService;
import com.eiztracking.demo.service.DictService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author ludonglin
 * @creat 2020-05-08-14:32
 */
@SpringBootTest
@AutoConfigureMockMvc
public class JpaTests {



    @Autowired
    DictDataService dictDataService;

    @Autowired
    DictService dictService;


    @Test
    void testSave(){

        //第一种写法
//        Dict dict = new Dict();
//        dict.setDictValue(6);

//        DictData dictData = new DictData();
//        dictData.setDict(dict);
//        dictData.setDictDataValue(13);
//
//        dictData.setDictDataName("war of warcraft3");
//        dictDataService.insertDictData(dictData);

        //第二种
//        Dict dict1 = new Dict();
//        dict1.setDictValue(6);
//        dict1.setDictName("Film");
//
//        DictData dictData2 = new DictData();
//        dictData2.setDictDataValue(13);
//        dictData2.setDictDataName("war of warcraft2");
//
//        dict1.addDictData(dictData2);
//        dictService.insertDict(dict1);


//        Dict dict = new Dict();
//        dict.setDict_value(6);
//        dict.setDict_name("Film");
//
//        DictData dictData = new DictData();
//        dictData.setDict_data_value(13);
//        dictData.setDict_data_name("war of warcraft2");
//
//        dictData.setDict(dict);
//        dict.getDictdata().add(dictData);
//
//        dictService.insertDict(dict);


        Dict dict1 = new Dict();
        dict1.setDictValue(6);

        DictData dictData2 = new DictData();
        dictData2.setDictDataValue(13);
        dictData2.setDictDataName("war of warcraft888");

        dictDataService.insertDictData(dictData2,dict1.getDictValue());


    }


}
