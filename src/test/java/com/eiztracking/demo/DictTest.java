package com.eiztracking.demo;

import com.eiztracking.demo.entity.PO.Dict;
import com.eiztracking.demo.service.DictService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author ludonglin
 * @creat 2020-05-13-11:38
 */
@SpringBootTest
public class DictTest {

    @Autowired
    DictService dictService;

    @Test
    void testSave(){

        Dict dict = new Dict();
        dict.setDictValue(7);
        dict.setDictName("Toy");


        dictService.insertDict(dict);
    }


}
