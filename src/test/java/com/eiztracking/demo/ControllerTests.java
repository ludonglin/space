package com.eiztracking.demo;

import com.eiztracking.demo.controller.AccountController;
import com.eiztracking.demo.exception.AccountException;
import com.eiztracking.demo.service.AccountService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ludonglin
 * @creat 2020-05-12-9:56
 */
@SpringBootTest

public class ControllerTests {

    @Autowired
    AccountController accountController;

    @Autowired
    AccountService accountService;


    private  final  static String url =  "http://localhost:8080";

    private static RestTemplate restTemplate = new RestTemplate();


    //account insert
    @Test
    void testController() throws AccountException {
        MultiValueMap<String, Object> paramMap = new LinkedMultiValueMap<String, Object>();
        paramMap.add("eizAccountId", "6");
        paramMap.add("email", "ludonglinr@163.com");
        paramMap.add("name", "ldl");
        paramMap.add("phone", "18710737112");
        paramMap.add("createdAt", "2020-05-13");
        paramMap.add("status", "1");

        String result = restTemplate.postForObject(url + "/insertAccount", paramMap, String.class);
        System.out.println("result: " + result);
    }

    @Test
    public void testfindAccountById() throws Exception{
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("id", "1");
        String result1  = restTemplate.getForObject(url+"/findAccountById/{id}",String.class,paramMap);
        System.out.println("result: " + result1);
    }



}
