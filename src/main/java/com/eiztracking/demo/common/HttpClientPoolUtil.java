package com.eiztracking.demo.common;

import com.eiztracking.demo.exception.CourierException;
import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.*;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;

import javax.net.ssl.SSLContext;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

public class HttpClientPoolUtil {

    private static Logger logger = LoggerFactory.getLogger(HttpClientPoolUtil.class);

    public static PoolingHttpClientConnectionManager cm = null;

    public static CloseableHttpClient httpClient = null;

    /**
     * 默认content 类型
     */
    private static final String DEFAULT_CONTENT_TYPE = "application/json";

    /**
     * 默认请求超时时间30s
     */
    @Value("${httpclient.http.default.timeout}")
    private static Integer DEFAULT_TIME_OUT = 300;

    @Value("${httpclient.cm.default.max.per.route}")
    private static Integer COUNT = 32;

    @Value("${httpclient.cm.maxtotal}")
    private static Integer MAXTOTAL = 100;

    @Value("${httpclient.http.default.keep.time}")
    private static Integer HTTP_DEFAULT_KEEP_TIME = 15000;

    /**
     * 初始化连接池
     */
    public static synchronized void initPools() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        if (httpClient == null) {
            TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
            SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext,
                    NoopHostnameVerifier.INSTANCE);

            Registry<ConnectionSocketFactory> socketFactoryRegistry =
                    RegistryBuilder.<ConnectionSocketFactory> create()
                            .register("https", sslsf)
                            .register("http", new PlainConnectionSocketFactory())
                            .build();

//            BasicHttpClientConnectionManager connectionManager =
//                    new BasicHttpClientConnectionManager(socketFactoryRegistry);

            cm = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
            cm.setDefaultMaxPerRoute(COUNT);
            cm.setMaxTotal(MAXTOTAL);
            httpClient = HttpClients.custom().setSSLSocketFactory(sslsf)
//                    .setConnectionManager(connectionManager)
                    .setKeepAliveStrategy(defaultStrategy)
                    .setConnectionManager(cm)
                    .build();
        }
    }

    /**
     * Http connection keepAlive 设置
     */
    public static ConnectionKeepAliveStrategy defaultStrategy = new ConnectionKeepAliveStrategy() {
        public long getKeepAliveDuration(HttpResponse response, HttpContext context) {
            HeaderElementIterator it = new BasicHeaderElementIterator(response.headerIterator(HTTP.CONN_KEEP_ALIVE));
            int keepTime = HTTP_DEFAULT_KEEP_TIME;
            while (it.hasNext()) {
                HeaderElement he = it.nextElement();
                String param = he.getName();
                String value = he.getValue();
                if (value != null && param.equalsIgnoreCase("timeout")) {
                    try {
                        return Long.parseLong(value) * 1000;
                    } catch (Exception e) {
                        e.printStackTrace();
                        logger.error("format KeepAlive timeout exception, exception:" + e.toString());
                    }
                }
            }
            return keepTime * 1000;
        }
    };


    public static CloseableHttpClient getHttpClient() {
        return httpClient;
    }

    public static PoolingHttpClientConnectionManager getHttpConnectionManager() {
        return cm;
    }

    /**
     * 创建请求
     *
     * @param uri 请求url
     * @param methodName 请求的方法类型
     * @param contentType contentType类型
     * @param timeout 超时时间
     * @return
     */
    public static HttpRequestBase buildRequest(String uri, String methodName, String contentType, int timeout) throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        if (httpClient == null) {
            initPools();
        }

        HttpRequestBase method = null;
        if (timeout <= 0) {
            timeout = DEFAULT_TIME_OUT;
        }
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(timeout * 1000).setConnectTimeout(timeout * 1000)
                .setConnectionRequestTimeout(timeout * 1000).setExpectContinueEnabled(false).build();

        if (HttpPut.METHOD_NAME.equalsIgnoreCase(methodName)) {
            method = new HttpPut(uri);
        } else if (HttpPost.METHOD_NAME.equalsIgnoreCase(methodName)) {
            method = new HttpPost(uri);
        } else if (HttpGet.METHOD_NAME.equalsIgnoreCase(methodName)) {
            method = new HttpGet(uri);
        } else {
            method = new HttpPost(uri);
        }
        if (StringUtils.isEmpty(contentType)) {
            contentType = DEFAULT_CONTENT_TYPE;
        }
        method.addHeader("Content-Type", contentType);
//        method.addHeader("Accept", contentType);
        method.setConfig(requestConfig);
        return method;
    }

    /**
     * 执行http post请求 默认采用Content-Type：application/json，Accept：application/json
     *
     * @param uri
     * @param data
     * @param contentType
     * @return
     */
    public static String post(String uri, String data, String contentType, Map<String, String> httpHeaderMap) {
        long startTime = System.currentTimeMillis();
        HttpEntity httpEntity = null;
        HttpEntityEnclosingRequestBase method = null;
        String responseBody = null;
        try {
            if (httpClient == null) {
                initPools();
            }

            if (contentType == null) {
                contentType = DEFAULT_CONTENT_TYPE;
            }

            method = (HttpEntityEnclosingRequestBase) buildRequest(uri, HttpPost.METHOD_NAME, contentType, DEFAULT_TIME_OUT);

            if (httpHeaderMap != null && !httpHeaderMap.isEmpty()) {
                for (Map.Entry<String, String> entry : httpHeaderMap.entrySet()) {
                    method.addHeader(entry.getKey(), entry.getValue());
                }
            }

            if (data != null)
                method.setEntity(new StringEntity(data));

            HttpContext context = HttpClientContext.create();
            CloseableHttpResponse httpResponse = httpClient.execute(method, context);
            httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {
                responseBody = EntityUtils.toString(httpEntity, "UTF-8");
                logger.debug("请求URL: "+uri+"+  返回状态码："+httpResponse.getStatusLine().getStatusCode());
            }

        } catch (Exception e) {
            if(method != null){
                method.abort();
            }
            logger.error(
                    "execute post request exception, url:" + uri + ", exception:" + e.toString() + ", cost time(ms):"
                            + (System.currentTimeMillis() - startTime));
        } finally {
            if (httpEntity != null) {
                try {
                    EntityUtils.consumeQuietly(httpEntity);
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.error(
                            "close response exception, url:" + uri + ", exception:" + e.toString() + ", cost time(ms):"
                                    + (System.currentTimeMillis() - startTime));
                }
            }
        }
        return responseBody;
    }

    /**
     * 执行GET 请求
     *
     * @param uri
     * @return
     */
    public static String get(String uri, Map<String, String> httpHeaderMap) {
        long startTime = System.currentTimeMillis();
        HttpEntity httpEntity = null;
        HttpRequestBase method = null;
        String responseBody = null;
        try {
            if (httpClient == null) {
                initPools();
            }
            method = buildRequest(uri, HttpGet.METHOD_NAME, DEFAULT_CONTENT_TYPE, DEFAULT_TIME_OUT);

            if (httpHeaderMap != null && !httpHeaderMap.isEmpty()) {
                for (Map.Entry<String, String> entry : httpHeaderMap.entrySet()) {
                    method.addHeader(entry.getKey(), entry.getValue());
                }
            }

            HttpContext context = HttpClientContext.create();
            CloseableHttpResponse httpResponse = httpClient.execute(method, context);

            httpEntity = httpResponse.getEntity();
            int statusCode = httpResponse.getStatusLine().getStatusCode();

            if (statusCode != 200) {
                if (httpEntity != null) {
                    responseBody = EntityUtils.toString(httpEntity, "UTF-8");
//                    logger.error("请求URL: "+uri+"+  返回状态码："+httpResponse.getStatusLine().getStatusCode());
                }
                throw new CourierException(responseBody);
            }

            if (httpEntity != null) {
                responseBody = EntityUtils.toString(httpEntity, "UTF-8");
                logger.debug("请求URL: "+uri+"+  返回状态码："+httpResponse.getStatusLine().getStatusCode());
            }
        } catch (Exception e) {
            if(method != null){
                method.abort();
            }

            logger.error("execute get request exception, url:" + uri + ", exception:" + e.toString() + ",cost time(ms):"
                    + (System.currentTimeMillis() - startTime));
        } finally {
            if (httpEntity != null) {
                try {
                    EntityUtils.consumeQuietly(httpEntity);
                } catch (Exception e) {
                    logger.error("close response exception, url:" + uri + ", exception:" + e.toString() + ",cost time(ms):"
                            + (System.currentTimeMillis() - startTime));
                }
            }
        }
        return responseBody;
    }

    public static String buildURL(String url, Map<String, String> parameters) {

        URIBuilder builder = null;
        try {
            builder = new URIBuilder(url);

            if(parameters != null && !parameters.isEmpty() && builder != null)
            {
                for (Map.Entry<String, String> entry : parameters.entrySet())
                {
                    String key = entry.getKey();
                    if(!StringUtils.isEmpty(key))
                    {
                        String value = entry.getValue();
                        builder.addParameter(key, value);
                        logger.debug("Added Parameter: key=" + key + ", value=" + value);
                    }
                }

                return builder.build().toString();
            }

        } catch (URISyntaxException e) {
            logger.error(e.getMessage());
        }

        return null;
    }

}
