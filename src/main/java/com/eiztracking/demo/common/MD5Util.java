package com.eiztracking.demo.common;

import org.apache.tomcat.util.codec.binary.Base64;

public class MD5Util {

	public static String MD5Encode(String content) {
		String sign = "";
		String charset = "UTF-8";

		try {
//			MessageDigest md = MessageDigest.getInstance("MD5");
//			md.update(content.getBytes(charset));
//			sign = new String(Base64.encodeBase64(content.getBytes()), charset);
			sign = Base64.encodeBase64String(content.getBytes());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return sign;
	}
	
	public static void main(String[] args) {
		
		String content = "601a4032-6dbd-46aa-9c6c-8c6dacca5e61:password";
		
		try {
			System.out.println(MD5Util.MD5Encode(content));
//			System.out.println(MD5Util.MD5Encode(content, "x82c53145004d556952f"));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}
}
