package com.eiztracking.demo.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DateTimeUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(DateTimeUtils.class);

    private final static SimpleDateFormat DayDf = new SimpleDateFormat("yyyy-MM-dd");
    private final static SimpleDateFormat YearMonthDf = new SimpleDateFormat("yyyy-MM");//格式化为年月
    private final static SimpleDateFormat DayMonthYearDf = new SimpleDateFormat("dd/MM/yyyy");//格式化为年月

    public static Date getZeroHourOfToday() {

        Calendar calender = Calendar.getInstance();
        calender.set(Calendar.HOUR_OF_DAY, 0);
        calender.set(Calendar.MINUTE, 0);
        calender.set(Calendar.SECOND, 0);

        return calender.getTime();
    }

    public static Date getZeroHourOfSelected(Date date) {


        Calendar calender = Calendar.getInstance();
        calender.setTime(date);
        calender.set(Calendar.HOUR_OF_DAY, 0);
        calender.set(Calendar.MINUTE, 0);
        calender.set(Calendar.SECOND, 0);

        return calender.getTime();
    }

    public static Date getNow() {

        Calendar calender = Calendar.getInstance();
        return calender.getTime();
    }

    public static long getTimestampInHours(int hours) {

        Calendar calender = Calendar.getInstance();
        calender.setTime(calender.getTime());
        calender.add(Calendar.HOUR_OF_DAY, hours);

        return calender.getTimeInMillis() / 1000;
    }

    /**
     * 获取时间段内所有的年月集合
     *
     * @param minDate 最小时间  2017-01
     * @param maxDate 最大时间 2017-10
     * @return 日期集合 格式为 年-月
     * @throws Exception
     */
    public static List<String> getMonthBetween(String minDate, String maxDate) throws Exception {
        List<String> result = new ArrayList<String>();


        Calendar min = Calendar.getInstance();
        Calendar max = Calendar.getInstance();

        min.setTime(YearMonthDf.parse(minDate));
        min.set(min.get(Calendar.YEAR), min.get(Calendar.MONTH), 1);

        max.setTime(YearMonthDf.parse(maxDate));
        max.set(max.get(Calendar.YEAR), max.get(Calendar.MONTH), 2);

        Calendar curr = min;
        while (curr.before(max)) {
            result.add(YearMonthDf.format(curr.getTime()));
            curr.add(Calendar.MONTH, 1);
        }

        return result;
    }

    /**
     * 获取当前时间之前一年所有的年月集合
     *
     * @return 日期集合 格式为 年-月
     * @throws Exception
     */
    public static List<String> getMonthWithinOneYear() throws Exception {
        List<String> result = new ArrayList<String>();

        Calendar min = Calendar.getInstance();
        Calendar max = Calendar.getInstance();

        min.setTime(new Date());
        min.add(Calendar.YEAR, -1);
        min.add(Calendar.MONTH, 1);
        min.set(min.get(Calendar.YEAR), min.get(Calendar.MONTH), 1);

        max.setTime(new Date());
        max.set(max.get(Calendar.YEAR), max.get(Calendar.MONTH), 1);

        Calendar curr = min;
        while (curr.compareTo(max) <= 0) {
            result.add(YearMonthDf.format(curr.getTime()));
            curr.add(Calendar.MONTH, 1);
        }

        return result;
    }

    /**
     * 获取指定年份的所有的年月集合
     *
     * @return 日期集合 格式为 年-月
     * @throws Exception
     */
    public static List<String> getMonthInYear(int year) throws Exception {
        List<String> result = new ArrayList<String>();

        Calendar min = Calendar.getInstance();
        Calendar max = Calendar.getInstance();

        min.set(year, 0, 1);

        max.set(year, 11, 1);

        Calendar curr = min;
        while (curr.compareTo(max) <= 0) {
            result.add(YearMonthDf.format(curr.getTime()));
            curr.add(Calendar.MONTH, 1);
        }

        return result;
    }

//    public static long getNextHourTimeMillis() {
//        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("AEDT"));
//        calendar.add(Calendar.HOUR_OF_DAY, 1);
//
//        return calendar.getTimeInMillis()/1000;
//    }

    public static long getNextTwoHourTimeMillis() {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("AEDT"));
        calendar.add(Calendar.HOUR_OF_DAY, 2);

        return calendar.getTimeInMillis()/1000;
    }

    public static String getNextWorkingDate() {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("AEDT"));
        calendar.add(Calendar.DAY_OF_WEEK, 1);

        // check if the date after addition is a working day.
        // If not then keep on incrementing it till it is a working day
        while(!isWorkingDay(calendar.getTime())) {
            calendar.add(Calendar.DAY_OF_WEEK, 1);
        }

        LOGGER.info("Next working day is " + DayMonthYearDf.format(calendar.getTime()));
        return DayMonthYearDf.format(calendar.getTime());
    }

    private static boolean isWorkingDay(Date date) {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("AEDT"));
        calendar.setTime(date);
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        // check if it is Saturday(day=7) or Sunday(day=1)
        if ((dayOfWeek == Calendar.SATURDAY) || (dayOfWeek == Calendar.SUNDAY)) {
            return false;
        }
        return true;
    }

    public static boolean isTheTimeBeforeCurrentTime(String expiryTime) {

        DateFormat formatterUTC = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        formatterUTC.setTimeZone(TimeZone.getTimeZone("UTC"));

        try {
            Date expiryUTCTime = formatterUTC.parse(expiryTime);
            Date now = getNow();

            long localTimeInMillis=now.getTime();
            /** long时间转换成Calendar */
            Calendar calendar= Calendar.getInstance();
            calendar.setTimeInMillis(localTimeInMillis);
            /** 取得时间偏移量 */
            int zoneOffset = calendar.get(Calendar.ZONE_OFFSET);
            /** 取得夏令时差 */
            int dstOffset = calendar.get(Calendar.DST_OFFSET);
            /** 从本地时间里扣除这些差量，即可以取得UTC时间*/
            calendar.add(Calendar.MILLISECOND, -(zoneOffset + dstOffset));
            /** 取得的时间就是UTC标准时间 */
            Date utcNow=new Date(calendar.getTimeInMillis());

            return expiryUTCTime.before(utcNow);

        } catch (ParseException e) {
            throw new RuntimeException("Expiry Time format is incorrect");
        }
    }

    public static Date getUTCTime(){
        Calendar cal = Calendar.getInstance();
        //获得时区和 GMT-0 的时间差,偏移量
        int offset = cal.get(Calendar.ZONE_OFFSET);
        //获得夏令时  时差
        int dstoff = cal.get(Calendar.DST_OFFSET);
        cal.add(Calendar.MILLISECOND, - (offset + dstoff));
        return cal.getTime();

    }

    public static Date convertToUTCDatetime(Date date) {

        Calendar calendar= Calendar.getInstance();
        calendar.setTimeInMillis(date.getTime());
        /** 取得时间偏移量 */
        int zoneOffset = calendar.get(Calendar.ZONE_OFFSET);
        /** 取得夏令时差 */
        int dstOffset = calendar.get(Calendar.DST_OFFSET);
        /** 从本地时间里扣除这些差量，即可以取得UTC时间*/
        calendar.add(Calendar.MILLISECOND, -(zoneOffset + dstOffset));
        /** 取得的时间就是UTC标准时间 */
        Date utcDate = new Date(calendar.getTimeInMillis());
        return utcDate;

    }

    public static Date getTheDateBeforeNow(int days) {

        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, - days);
        Date d = c.getTime();

        return d;

    }

    /**
     * 函数功能描述:获取本地时区的表示(比如:第八区-->+08:00)
     * @return
     */
    public static String getTimeZoneByNumExpress(){
        Calendar cal = Calendar.getInstance();
        TimeZone timeZone = cal.getTimeZone();
        int rawOffset = timeZone.getRawOffset();
        int timeZoneByNumExpress = rawOffset/3600/1000;
        String timeZoneByNumExpressStr = "";
        if(timeZoneByNumExpress > 0 && timeZoneByNumExpress < 10){
            timeZoneByNumExpressStr = "+" + "0" + timeZoneByNumExpress + ":" + "00";
        }
        else if(timeZoneByNumExpress >= 10){
            timeZoneByNumExpressStr = "+" + timeZoneByNumExpress + ":" + "00";
        }
        else if(timeZoneByNumExpress > -10 && timeZoneByNumExpress < 0){
            timeZoneByNumExpress = Math.abs(timeZoneByNumExpress);
            timeZoneByNumExpressStr = "-" + "0" + timeZoneByNumExpress + ":" + "00";
        }else if(timeZoneByNumExpress <= -10){
            timeZoneByNumExpress = Math.abs(timeZoneByNumExpress);
            timeZoneByNumExpressStr = "-" + timeZoneByNumExpress + ":" + "00";
        }else{
            timeZoneByNumExpressStr = "Z";
        }
        return timeZoneByNumExpressStr;
    }

    public static void main(String[] agrs) throws Exception {

//        Date utcdate = DateTimeUtils.getTheDateBeforeNow(10);
//
//
//
//        System.out.println(utcdate.getTime());
//        System.out.println(DateTimeUtils.getNow().getTime());
//        System.out.println((DateTimeUtils.getNow().getTime() - utcdate.getTime()));
//        System.out.println((DateTimeUtils.getNow().getTime() - utcdate.getTime()) / (24*60*60*1000));
//
//        int day = (int)((DateTimeUtils.getNow().getTime() - utcdate.getTime()) / (24*60*60*1000));

        System.out.println(DateTimeUtils.getTimeZoneByNumExpress());

    }


}
