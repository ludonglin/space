package com.eiztracking.demo.entity.response;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author ludonglin
 * @creat 2020-05-18-14:50
 */
@Data
public class Itemscoupons  {

    private String imageURL;
    private String Item;
    private List<Trackinginfo> trackinginfo;

    public Itemscoupons(){}


    public Itemscoupons(String imageURL, String item, List<Trackinginfo> trackinginfo) {
        this.imageURL = imageURL;
        this.Item = item;
        this.trackinginfo = trackinginfo;
    }
}
