package com.eiztracking.demo.entity.response;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author ludonglin
 * @creat 2020-05-18-20:08
 */
@Data
public class ConsignmentInfos  {

    private List<ConsignmentInfo> consignmentInfo;



}
