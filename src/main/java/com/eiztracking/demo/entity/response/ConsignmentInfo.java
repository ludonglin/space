package com.eiztracking.demo.entity.response;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author ludonglin
 * @creat 2020-05-18-14:50
 */
@Data
public class ConsignmentInfo  {


    private String POD;
    private String consignmentCode;
    private String deadWeight;
    private String deliveredAt;
    private String deliveryETA;
    private String status;
    private String volume;
    private List<Itemscoupons> itemscoupons;


}
