package com.eiztracking.demo.entity.response;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class Trackinginfo  {

    private String action;
    private String contractor;
    private String date;
    private String time;

    public Trackinginfo() {}

    public Trackinginfo(String action, String contractor, String date, String time) {
        this.action = action;
        this.contractor = contractor;
        this.date = date;
        this.time = time;
    }
}
