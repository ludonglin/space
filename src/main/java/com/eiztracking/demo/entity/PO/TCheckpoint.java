package com.eiztracking.demo.entity.PO;


import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "t_checkpoint")
public class TCheckpoint {

    private Integer id;
    private Date checkpointTime;
    private String location;
    private String country;
    private String countryAlpha2;
    private String message;
    private Date createdAt;
    private Integer trackingStatus;
    private String statusString;
    private TTracking tTracking;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "checkpoint_time", nullable = true)
    public Date getCheckpointTime() {
        return checkpointTime;
    }

    public void setCheckpointTime(Date checkpointTime) {
        this.checkpointTime = checkpointTime;
    }

    @Basic
    @Column(name = "location", nullable = true, length = 256)
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Basic
    @Column(name = "country", nullable = true, length = 256)
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Basic
    @Column(name = "country_alpha_2", nullable = true, length = 2)
    public String getCountryAlpha2() {
        return countryAlpha2;
    }

    public void setCountryAlpha2(String countryAlpha2) {
        this.countryAlpha2 = countryAlpha2;
    }

    @Basic
    @Column(name = "message", nullable = false, length = 512)
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Basic
    @Column(name = "created_at", nullable = true)
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Basic
    @Column(name = "status_string", nullable = true)
    public String getStatusString() {
        return statusString;
    }

    public void setStatusString(String statusString) {
        this.statusString = statusString;
    }

    @Basic
    @Column(name = "tracking_status", nullable = true)
    public Integer getTrackingStatus() {
        return trackingStatus;
    }

    public void setTrackingStatus(Integer trackingStatus) {
        this.trackingStatus = trackingStatus;
    }


    @ManyToOne
    @JoinColumn(name = "tracking_id", referencedColumnName = "id", nullable = false)
    public TTracking gettTracking() {
        return tTracking;
    }

    public void settTracking(TTracking tTracking) {
        this.tTracking = tTracking;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TCheckpoint that = (TCheckpoint) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(checkpointTime, that.checkpointTime) &&
                Objects.equals(location, that.location) &&
                Objects.equals(country, that.country) &&
                Objects.equals(countryAlpha2, that.countryAlpha2) &&
                Objects.equals(message, that.message) &&
                Objects.equals(createdAt, that.createdAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, checkpointTime, location, country, countryAlpha2, message, createdAt);
    }
}
