package com.eiztracking.demo.entity.PO;


import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * @author ludonglin
 * @creat 2020-05-07-10:40
 */
@Table(name = "t_account")
@Entity
public class Account {

    private Integer id;
    private Integer eizAccountId;
    private String email;
    private String name;
    private String phone;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createdAt;
    private int status;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Integer getEizAccountId() {
        return eizAccountId;
    }

    public void setEizAccountId(Integer eizAccountId) {
        this.eizAccountId = eizAccountId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Account() {
    }

    public Account(Integer id, Integer eizAccountId, String email, String name, String phone, Date createdAt, int status) {
        this.id = id;
        this.eizAccountId = eizAccountId;
        this.email = email;
        this.name = name;
        this.phone = phone;
        this.createdAt = createdAt;
        this.status = status;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", eizAccountId=" + eizAccountId +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", createdAt=" + createdAt +
                ", status=" + status +
                '}';
    }
}

