package com.eiztracking.demo.entity.PO;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ludonglin
 * @creat 2020-05-07-14:18
 */
@Table(name = "t_dict")
@Entity
public class Dict implements Serializable{


    private static final long serialVersionUID = -2744717962474204266L;
    @Id
    @Column(name = "dict_value")
    private Integer dictValue;

    @Column(name="dict_name")
    private String dictName;


    //一对多，EAGER表示已把关联的表的数据也拿出来
    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER, mappedBy="Dict")
    private List<DictData> dictdata = new ArrayList<>();

    public Dict() {
    }

    public Dict(Integer dictValue, String dictName) {
        this.dictValue = dictValue;
        this.dictName = dictName;
    }

    public Integer getDictValue() {
        return dictValue;
    }

    public void setDictValue(Integer dictValue) {
        this.dictValue = dictValue;
    }

    public String getDictName() {
        return dictName;
    }

    public void setDictName(String dictName) {
        this.dictName = dictName;
    }

    public List<DictData> getDictdata() {
        return dictdata;
    }

    public void setDictdata(List<DictData> dictdata) {
        this.dictdata = dictdata;
    }

    @Override
    public String toString() {
        return "Dict{" +
                "dict_value=" + dictValue +
                ", dict_name='" + dictName + '\'' +
                '}';
    }


    public DictData addDictData(DictData dictData) {
        dictData.setDict(this);
        getDictdata().add(dictData);
        return dictData;
    }


    public DictData removeDictData(DictData dictData) {
        dictData.setDict(null);
        getDictdata().remove(dictData);
        return dictData;
    }

}
