package com.eiztracking.demo.entity.PO;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author ludonglin
 * @creat 2020-05-07-14:18
 */
@Table(name = "t_dict_data")
@Entity
public class DictData implements Serializable{

    private static final long serialVersionUID = 255295259368335836L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

//    @Column(name="dict_value")
//    private Integer dict_value;

    @Column(name="dict_data_value")
    private Integer dictDataValue;

    @Column(name="dict_data_name")
    private String dictDataName;

    //表示外键为dict表中的dict_value，于dict中的dict_value关联
    @ManyToOne
    @JoinColumn(name="dict_value",referencedColumnName = "dict_value")
    private Dict Dict;

    public DictData() {
    }


    public Integer getId(Integer dictValue) {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDictDataValue() {
        return dictDataValue;
    }

    public void setDictDataValue(Integer dictDataValue) {
        this.dictDataValue = dictDataValue;
    }

    public String getDictDataName() {
        return dictDataName;
    }

    public void setDictDataName(String dictDataName) {
        this.dictDataName = dictDataName;
    }

    public Dict getDict() {
        return Dict;
    }

    public void setDict(Dict dict) {
        Dict = dict;
    }
}
