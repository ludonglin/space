package com.eiztracking.demo.entity.PO;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "t_tracking")
public class TTracking {

    private Integer id;
    private String labelNumber;
    private Integer eizAccountId;
    private Integer eizConsignmentId;
    private Integer trackingResultsNumber;
    private String slug;
    private Date createdAt;
    private Date updatedAt;
    private long nextTrackingTime;
    private Integer serviceProvider;
    private Integer status;
    private Integer trackingStatus;
    private String statusString;
    private Date lastCheckpointTime;
    private List<TCheckpoint> tCheckpoints = new ArrayList();

    private String fromPostcode;
    private String fromSuburb;
    private String fromState;
    private String fromCountry;
    private String toPostcode;
    private String toSuburb;
    private String toState;
    private String toCountry;

    private Integer shippingDays;

    public TTracking() {}

    public TTracking(String labelNumber) {
        this.labelNumber = labelNumber;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "label_number", nullable = false, length = 45)
    public String getLabelNumber() {
        return labelNumber;
    }

    public void setLabelNumber(String labelNumber) {
        this.labelNumber = labelNumber;
    }

    @Basic
    @Column(name = "eiz_account_id", nullable = true)
    public Integer getEizAccountId() {
        return eizAccountId;
    }

    public void setEizAccountId(Integer eizAccountId) {
        this.eizAccountId = eizAccountId;
    }

    @Basic
    @Column(name = "eiz_consignment_id", nullable = true)
    public Integer getEizConsignmentId() {
        return eizConsignmentId;
    }

    public void setEizConsignmentId(Integer eizConsignmentId) {
        this.eizConsignmentId = eizConsignmentId;
    }

    @Basic
    @Column(name = "tracking_results_number", nullable = false)
    public Integer getTrackingResultsNumber() {
        return trackingResultsNumber;
    }

    public void setTrackingResultsNumber(Integer trackingResultsNumber) {
        this.trackingResultsNumber = trackingResultsNumber;
    }

    @Basic
    @Column(name = "slug", nullable = true)
    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    @Basic
    @Column(name = "created_at", nullable = false)
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Basic
    @Column(name = "updated_at", nullable = true)
    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Basic
    @Column(name = "next_tracking_time", nullable = true)
    public long getNextTrackingTime() {
        return nextTrackingTime;
    }

    public void setNextTrackingTime(long nextTrackingTime) {
        this.nextTrackingTime = nextTrackingTime;
    }

    @Basic
    @Column(name = "service_provider", nullable = false)
    public Integer getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(Integer serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    @Basic
    @Column(name = "status_string", nullable = true)
    public String getStatusString() {
        return statusString;
    }

    public void setStatusString(String statusString) {
        this.statusString = statusString;
    }

    @Basic
    @Column(name = "tracking_status", nullable = false)
    public Integer getTrackingStatus() {
        return trackingStatus;
    }

    public void setTrackingStatus(Integer trackingStatus) {
        this.trackingStatus = trackingStatus;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "last_checkpoint_time", nullable = true)
    public Date getLastCheckpointTime() {
        return lastCheckpointTime;
    }

    public void setLastCheckpointTime(Date lastCheckpointTime) {
        this.lastCheckpointTime = lastCheckpointTime;
    }

    @Basic
    @Column(name = "from_postcode", nullable = true)
    public String getFromPostcode() {
        return fromPostcode;
    }

    public void setFromPostcode(String fromPostcode) {
        this.fromPostcode = fromPostcode;
    }

    @Basic
    @Column(name = "from_suburb", nullable = true)
    public String getFromSuburb() {
        return fromSuburb;
    }

    public void setFromSuburb(String fromSuburb) {
        this.fromSuburb = fromSuburb;
    }

    @Basic
    @Column(name = "from_state", nullable = true)
    public String getFromState() {
        return fromState;
    }

    public void setFromState(String fromState) {
        this.fromState = fromState;
    }

    @Basic
    @Column(name = "from_country", nullable = true)
    public String getFromCountry() {
        return fromCountry;
    }

    public void setFromCountry(String fromCountry) {
        this.fromCountry = fromCountry;
    }

    @Basic
    @Column(name = "to_postcode", nullable = true)
    public String getToPostcode() {
        return toPostcode;
    }

    public void setToPostcode(String toPostcode) {
        this.toPostcode = toPostcode;
    }

    @Basic
    @Column(name = "to_suburb", nullable = true)
    public String getToSuburb() {
        return toSuburb;
    }

    public void setToSuburb(String toSuburb) {
        this.toSuburb = toSuburb;
    }

    @Basic
    @Column(name = "to_state", nullable = true)
    public String getToState() {
        return toState;
    }

    public void setToState(String toState) {
        this.toState = toState;
    }

    @Basic
    @Column(name = "to_country", nullable = true)
    public String getToCountry() {
        return toCountry;
    }

    public void setToCountry(String toCountry) {
        this.toCountry = toCountry;
    }

    @Basic
    @Column(name = "shipping_days", nullable = true)
    public Integer getShippingDays() {
        return shippingDays;
    }

    public void setShippingDays(Integer shippingDays) {
        this.shippingDays = shippingDays;
    }

    @OneToMany(mappedBy = "tTracking", cascade = CascadeType.ALL)
    public List<TCheckpoint> gettCheckpoints() {
        return tCheckpoints;
    }

    public void settCheckpoints(List<TCheckpoint> tCheckpoints) {
        this.tCheckpoints = tCheckpoints;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TTracking tTracking = (TTracking) o;
        return Objects.equals(id, tTracking.id) &&
                Objects.equals(labelNumber, tTracking.labelNumber) &&
                Objects.equals(eizConsignmentId, tTracking.eizConsignmentId) &&
                Objects.equals(trackingResultsNumber, tTracking.trackingResultsNumber) &&
                Objects.equals(createdAt, tTracking.createdAt) &&
                Objects.equals(updatedAt, tTracking.updatedAt) &&
                Objects.equals(nextTrackingTime, tTracking.nextTrackingTime) &&
                Objects.equals(serviceProvider, tTracking.serviceProvider) &&
                Objects.equals(status, tTracking.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, labelNumber, eizConsignmentId, trackingResultsNumber, createdAt, updatedAt, nextTrackingTime, serviceProvider, status);
    }

    public void addTCheckpoint(TCheckpoint tCheckpoint) {
        this.tCheckpoints.add(tCheckpoint);
    }
}
