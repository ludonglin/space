package com.eiztracking.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class EiZtrackingApplication {

	public static void main(String[] args) {
		SpringApplication.run(EiZtrackingApplication.class, args);
	}

}
