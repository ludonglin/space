package com.eiztracking.demo.repository;

import com.eiztracking.demo.entity.PO.DictData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author ludonglin
 * @creat 2020-05-07-15:11
 */
@Component
public interface DictDataRepository extends JpaRepository<DictData,Integer> {

    @Query(value = "select dt.dict_value,dt.dict_data_value,dt.dict_data_name from t_dict_data dt,t_dict d where dt.dict_value=d.dict_value",nativeQuery = true)
    List<Map<String,Object>> getDictDataList();


    //根据dict_value查询多表
    @Query(value = "select d.dict_name,dt.dict_value,dt.dict_data_value,dt.dict_data_name from t_dict_data dt,t_dict d where dt.dict_value=d.dict_value and d.dict_value=(:dict_value)",nativeQuery = true)
    List<Map<String,Object>> getDictDataListByDict_Value(@Param("dict_value") Integer dict_value);


}
