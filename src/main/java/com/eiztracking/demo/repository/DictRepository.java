package com.eiztracking.demo.repository;

import com.eiztracking.demo.entity.PO.Dict;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author ludonglin
 * @creat 2020-05-07-14:26
 */
@Component
public interface DictRepository extends JpaRepository<Dict,Integer> {

    @Query(value = "select dt.dict_value,dt.dict_name from t_dict dt",nativeQuery = true)
    List<Map<String,Object>> getDictList();


    @Query(value = "select dt.dict_value,dt.dict_name from t_dict dt WHERE dt.dict_value=(:dict_value)",nativeQuery = true)
    List<Map<String,Object>> getDictByDictValue(@Param("dict_value") Integer dict_value);

}
