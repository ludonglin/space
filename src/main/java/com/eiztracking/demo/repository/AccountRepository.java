package com.eiztracking.demo.repository;

import com.eiztracking.demo.entity.PO.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author ludonglin
 * @creat 2020-05-07-10:57
 */
@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {

    //根据姓名查询数据
    public List<Account> findByName(String name);

}
