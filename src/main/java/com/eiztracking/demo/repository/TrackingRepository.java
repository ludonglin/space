package com.eiztracking.demo.repository;

import com.eiztracking.demo.entity.PO.TTracking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository
public interface TrackingRepository extends JpaRepository<TTracking, Integer> {

    Optional<TTracking> findByLabelNumber(String labelNumber);

    Optional<List<TTracking>> findTop15ByServiceProviderEqualsAndStatusEqualsAndNextTrackingTimeLessThanEqualOrderByNextTrackingTimeAsc(int courierType, int status, long currentMillisTime);

//    Optional<TTracking> findTop15ByServiceProviderEqualsAndStatusEqualsAndNextTrackingTimeLessThanEqualOrderByNextTrackingTimeAsc(int courierType, int status, long currentMillisTime);
}
