package com.eiztracking.demo.controller;

import com.eiztracking.demo.entity.PO.Account;
import com.eiztracking.demo.exception.AccountException;
import com.eiztracking.demo.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @author ludonglin
 * @creat 2020-05-07-11:23
 */
@RestController
@Slf4j
public class AccountController {


    @Autowired
    AccountService accountService;


    //添加一个账号
    @PostMapping(value = "/insertAccount")
    public Account insertAccount(Account account) throws AccountException{
        log.debug("insert account {}",account.toString());
        return accountService.insertAccount(account);
    }

    //根据id更新
    @PutMapping(value = "/updateAccount")
    public Account updateAccount(Account account)throws AccountException{
        return accountService.updateAccount(account);
    }

    @DeleteMapping(value = "/deleteAccount/{id}")
    public void deleteAccountById(@PathVariable(name = "id")Integer id)throws AccountException{
        accountService.deleteAccount(id);
    }

    //查询所有
    @GetMapping(value = "/getAll")
    public List<Account> getAll() throws AccountException{
        return accountService.getall();
    }

    @GetMapping(value = "/findAccountById/{id}")
    public Account findAccountById(@PathVariable(name = "id") Integer id)throws AccountException {
        return accountService.findAccountById(id);
    }


    //根据姓名查询多条数据
    @GetMapping(value = "/findAccountByName/{name}")
    public List<Account> findAccountByName(@PathVariable(name = "name") String name) throws AccountException{
        return accountService.findByName(name);
    }



}
