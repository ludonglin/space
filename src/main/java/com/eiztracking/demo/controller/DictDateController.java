package com.eiztracking.demo.controller;

import com.alibaba.fastjson.JSON;
import com.eiztracking.demo.entity.PO.DictData;
import com.eiztracking.demo.exception.DictDataException;
import com.eiztracking.demo.service.DictDataService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author ludonglin
 * @creat 2020-05-07-15:16
 */
@RestController
@Slf4j
public class DictDateController {


    @Autowired
    DictDataService dictDataService;


    /**
     * dict表和dict_data根据dict_value联合查询
     * @return
     */
    @GetMapping("/getDictDataList")
    public String getDictDataList() throws DictDataException{
        List<Map<String, Object>> list = dictDataService.getDictDataList();
        String jsonstr = JSON.toJSONString(list);
        return jsonstr;
    }

    @PostMapping("/getDictDataListByDict_Value/{dictValue}")
    public String getDictDataListByDict_Value(@PathVariable(name = "dictValue") Integer dictValue) throws DictDataException{
        List<Map<String, Object>> list = dictDataService.getDictDataListByDict_Value(dictValue);
        String jsonstr = JSON.toJSONString(list);
        return jsonstr;
    }


    @PostMapping(value = "/insertDictData/{dict_value}")
    public DictData insertDictData(DictData dictData, @PathVariable(name = "dict_value") Integer dictValue)  throws DictDataException {
        return dictDataService.insertDictData(dictData,dictValue);
    }


    @DeleteMapping(value = "/removeDictDataById/{id}")
    public void removeDictDataById(@PathVariable(name = "id") Integer id)  throws DictDataException {
        dictDataService.removeDictDataById(id);
    }



}
