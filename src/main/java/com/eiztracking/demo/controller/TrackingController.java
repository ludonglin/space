package com.eiztracking.demo.controller;

import com.eiztracking.demo.entity.response.BaseResponse;
import com.eiztracking.demo.entity.response.ConsignmentInfos;
import com.eiztracking.demo.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

@CrossOrigin
@RestController
@Slf4j
public class TrackingController {

    @Autowired
    TrackingService trackingService;


    @GetMapping(value = "/getTrackingDetail", params = {"labelNumber"})
    @ResponseBody
    public BaseResponse getTrackingDetail(HttpServletRequest request, @RequestParam @NotNull String labelNumber) {
        if (labelNumber == null || labelNumber.equals("")) {
            return BaseResponse.error(BaseResponse.INVALID_INPUT, BaseResponse.missIng);
        }
//        if(request.getHeader("")==null){
//            return BaseResponse.error(BaseResponse.UNAUTHORIZED,BaseResponse.noToken);
//        }
        ConsignmentInfos consignmentInfo = trackingService.getTrackingDetail(labelNumber);
        return BaseResponse.success(consignmentInfo);
    }


}
