package com.eiztracking.demo.controller;

import com.alibaba.fastjson.JSON;
import com.eiztracking.demo.entity.PO.Dict;
import com.eiztracking.demo.exception.DictException;
import com.eiztracking.demo.service.DictService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author ludonglin
 * @creat 2020-05-07-14:43
 */
@RestController
@Slf4j
public class DictController {


    @Autowired
    DictService dictService;


    @PostMapping(value = "/insertDict")
    public Dict insertDict(Dict dict) throws DictException {
        return dictService.insertDict(dict);
    }

    //根据id更新
    @PutMapping(value = "/updateDict")
    public Dict updateAccount(Dict dict)throws DictException{
        return dictService.updateDict(dict);
    }

    //设置级联后，删除主表从表也会被删除
    @DeleteMapping(value = "/deleteDict/{id}")
    public void deleteDictById(@PathVariable(name = "id")Integer id)throws DictException{
        dictService.deleteDict(id);
    }

    @GetMapping(value = "/getDictAll")
    public String getDictList()  throws  DictException{
        List<Map<String, Object>> dictDataList = dictService.getDictDataList();
        return JSON.toJSONString(dictDataList);
    }

    @GetMapping(value = "/findDictByDictValue/{dictvalue}")
    public List<Map<String,Object>> findAccountByDictValue(@PathVariable(name = "dictvalue") Integer dictvalue)throws DictException {
        List<Map<String, Object>> dict = dictService.getDictByDictValue(dictvalue);
        return dict;
    }


}
