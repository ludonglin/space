package com.eiztracking.demo.constant;

public class CourierConstant {

    public static final int COURIER_TYPE_EPARCEL = 1;
    public static final int COURIER_TYPE_FASTWAY = 2;
    public static final int COURIER_TYPE_TOLL = 3;
    public static final int COURIER_TYPE_DHL = 5;
    public static final int COURIER_TYPE_TNT = 6;
    public static final int COURIER_TYPE_COURIERSPLEASE = 8;
    public static final int COURIER_TYPE_ALLIEDEXPRESS = 9;
    public static final int COURIER_TYPE_ICUMULUS = 10;
    public static final int COURIER_TYPE_PFL = 11;
    public static final int COURIER_TYPE_AFTERSHIP = 99;


    public static final String FASTWAY_TRACKING_STATUS_P = "Picked up";
    public static final String FASTWAY_TRACKING_STATUS_T = "In transit";
    public static final String FASTWAY_TRACKING_STATUS_D = "Delivered";

    public static final String AFTERSHIP_SLUG_TOLL = "toll-ipec";
    public static final String AFTERSHIP_SLUG_TNT = "tnt-au";
    public static final String AFTERSHIP_SLUG_DHL = "dhl-global-mail-asia";
    public static final String AFTERSHIP_SLUG_COURIERSPLEASE ="couriers-please";
    public static final String AFTERSHIP_SLUG_ALLIEDEXPRESS ="alliedexpress";
}
