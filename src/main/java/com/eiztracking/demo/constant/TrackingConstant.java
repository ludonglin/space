package com.eiztracking.demo.constant;

public class TrackingConstant {

    public static final int TRACKING_JOB_STATUS_TRACKING = 1;
    public static final int TRACKING_JOB_STATUS_FINALIZED = 2;
    public static final int TRACKING_JOB_STATUS_EXCEPTION = 3;
    public static final int TRACKING_JOB_STATUS_DELETED = 9;

    public static final int TRACKING_STATUS_PENDING = 1;
    public static final int TRACKING_STATUS_INFORECEIVED = 2;
    public static final int TRACKING_STATUS_INTRANSIT = 3;
    public static final int TRACKING_STATUS_OUTFORDELIVERY = 4;
    public static final int TRACKING_STATUS_ATTEMPTFAIL = 5;
    public static final int TRACKING_STATUS_DELIVERED = 6;
    public static final int TRACKING_STATUS_EXCEPTION = 7;
    public static final int TRACKING_STATUS_EXPIRED = 8;
    public static final int TRACKING_STATUS_CANCELLED = 9;
    public static final int TRACKING_STATUS_AWAITING_COLLECTION = 10;

    public static final int TRACKING_STATUS = 1;






}
