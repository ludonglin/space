package com.eiztracking.demo.exception;

/**
 * @author ludonglin
 * @creat 2020-05-11-16:29
 */
public class AccountException extends RuntimeException{

    private static final long serialVersionUID = -2981134278687325103L;

    private Integer code;

    public AccountException() {
        super();
    }

    public AccountException(Integer code, String msg) {
        super(msg);
        this.setCode(code);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }


}
