package com.eiztracking.demo.exception;

/**
 * @author ludonglin
 * @creat 2020-05-12-9:40
 */
public class DictDataException extends RuntimeException{


    private static final long serialVersionUID = 4300930547113155913L;
    private Integer code;

    public DictDataException(Integer code, String msg) {
        super(msg);
        this.setCode(code);
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public DictDataException() {
        super();
    }
}
