package com.eiztracking.demo.exception;

import com.eiztracking.demo.entity.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.beans.Transient;
import java.util.Objects;

/**
 * @author ludonglin
 * @creat 2020-05-11-16:34
 */
public class ExceptionEntity<T> {
    public static final String INVALIDINPUT = "INVALID_INPUT";
    public static final String SUCCESS = "SUCCESS";
    public static final String INVALID_INPUT ="INVALID_INPUT";
    public static final String SERVICE_UNAVAILABLE ="SERVICE_UNAVAILABLE";
    private static final String missIng ="Tracking code missing";
    private static final String noToken ="Not a valid token";

    private String responseCode;
    private String msg;
    private T data;

    public ExceptionEntity() {
    }


    public ExceptionEntity(String responseCode, String msg) {
        this.responseCode = responseCode;
        this.msg = msg;
        this.data=null;
    }



    public ExceptionEntity(String responseCode, String msg, T data) {
        this.responseCode = responseCode;
        this.data = data;
        this.msg = msg;
    }

    public static <T> BaseResponse<T> success(T t) {
        BaseResponse<T> res = new BaseResponse();
        res.setResponseCode(SUCCESS);
        res.setMsg("");
        res.setData(t);
        return res;
    }

    public static <T> BaseResponse<T> error(String errorCde,String msg) {
        BaseResponse<T> res = new BaseResponse();
        res.setResponseCode(errorCde);
        res.setMsg(msg);
        res.setData(null);
        return res;
    }


    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }


    @Transient
    @JsonIgnore
    public boolean isSuccess() {
        return Objects.equals(this.responseCode, "1");
    }

    public String toString() {
        return "BaseResponse [code=" + this.responseCode + ", msg=" + this.msg + ", data=" + this.data + "]";
    }
}
