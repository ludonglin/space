package com.eiztracking.demo.exception;

/**
 * @author ludonglin
 * @creat 2020-05-19-21:30
 */
public class TrackingException extends RuntimeException {

    private static final long serialVersionUID = 2453999984846173279L;

    private String code;

    public TrackingException(String code,String msg) {
        super(msg);
        this.setCode(code);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public TrackingException() {
        super();
    }
}
