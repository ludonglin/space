package com.eiztracking.demo.exception;

import com.eiztracking.demo.entity.response.BaseResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author ludonglin
 * @creat 2020-05-11-16:24
 */
@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * AccountException
     * @param request
     * @param e
     * @param response
     * @return
     */
    @ExceptionHandler(AccountException.class)
    public ExceptionEntity AccountExceptionEntity(HttpServletRequest request, final Exception e, HttpServletResponse response){
        response.setStatus(HttpStatus.BAD_REQUEST.value());
        AccountException accountException=(AccountException)e;
        return new ExceptionEntity(String.valueOf(accountException.getCode()),accountException.getMessage());
    }


    /**
     * DictException
     * @param request
     * @param e
     * @param response
     * @return
     */
    @ExceptionHandler(DictException.class)
    public ExceptionEntity DictExceptionEntity(HttpServletRequest request, final Exception e, HttpServletResponse response){
        response.setStatus(HttpStatus.BAD_REQUEST.value());
        DictException dictException=(DictException)e;
        return new ExceptionEntity(String.valueOf(dictException.getCode()),dictException.getMessage());
    }

    /**
     * DictDataException
     * @param request
     * @param e
     * @param response
     * @return
     */
    @ExceptionHandler(DictDataException.class)
    public ExceptionEntity DictDataExceptionEntity(HttpServletRequest request, final Exception e, HttpServletResponse response){
        response.setStatus(HttpStatus.BAD_REQUEST.value());
        DictDataException dictdataException=(DictDataException)e;
        return new ExceptionEntity(String.valueOf(dictdataException.getCode()),dictdataException.getMessage());
    }


    /***
     * 可以自定义多个，来捕获不同的异常
     * @param request
     * @param e
     * @param response
     * @return
     */
    @ExceptionHandler(RuntimeException.class)
    public ExceptionEntity runtimeExceptionEntity(HttpServletRequest request,final Exception e,HttpServletResponse response){
        response.setStatus(HttpStatus.BAD_REQUEST.value());
        RuntimeException runtimeException=(RuntimeException)e;
        return new ExceptionEntity(String.valueOf(400),runtimeException.getMessage());
    }

    /***
     * Override ResponseEntityExceptionHandler类中的handleExceptionInternal方法
     * 通用的接口映射异常处理方
     * @param e
     * @param o
     * @param httpHeaders
     * @param httpStatus
     * @param webRequest
     * @return
     */
    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception e, Object o, HttpHeaders httpHeaders,
                                                             HttpStatus httpStatus, WebRequest webRequest){
        if (e instanceof MethodArgumentNotValidException){
            MethodArgumentNotValidException methodArgumentNotValidException=(MethodArgumentNotValidException)e;
            return new ResponseEntity<>(new ExceptionEntity(String.valueOf(httpStatus.value()),methodArgumentNotValidException.getBindingResult().getAllErrors().get(0).getDefaultMessage()),httpStatus);
        }
        if(e instanceof MethodArgumentTypeMismatchException){
            MethodArgumentTypeMismatchException methodArgumentTypeMismatchException=(MethodArgumentTypeMismatchException)e;
            logger.error("参数转换失败，方法："+methodArgumentTypeMismatchException.getParameter().getMethod().getName()+",参数："+
                    methodArgumentTypeMismatchException.getName()+"，信息："+methodArgumentTypeMismatchException.getLocalizedMessage());
            return new ResponseEntity<>(new ExceptionEntity(String.valueOf(httpStatus.value()),"参数转换失败"),httpStatus);
        }
        return new ResponseEntity<>(new ExceptionEntity(String.valueOf(httpStatus.value()),"参数转换失败"),httpStatus);
    }

    /**
     * DictDataException
     * @param request
     * @param e
     * @param response
     * @return
     */
    @ExceptionHandler(TrackingException.class)
    public BaseResponse TrackingException(HttpServletRequest request, final Exception e, HttpServletResponse response){
        return BaseResponse.error(BaseResponse.SERVICE_UNAVAILABLE,BaseResponse.SERVICE_UNAVAILABLE);
    }





}
