package com.eiztracking.demo.exception;

/**
 * @author ludonglin
 * @creat 2020-05-12-9:36
 */
public class DictException extends RuntimeException{


    private static final long serialVersionUID = 2453999984846173279L;

    private Integer code;

    public DictException(Integer code, String msg) {
        super(msg);
        this.setCode(code);
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public DictException() {
        super();
    }
}
