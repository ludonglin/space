package com.eiztracking.demo.service;

import com.eiztracking.demo.entity.PO.Dict;
import com.eiztracking.demo.entity.PO.DictData;
import com.eiztracking.demo.exception.DictException;
import com.eiztracking.demo.repository.DictDataRepository;
import com.eiztracking.demo.repository.DictRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author ludonglin
 * @creat 2020-05-07-15:12
 */
@Service
@Transactional
@Slf4j
public class DictDataService {

    @Autowired
    DictDataRepository dictDataRepository;

    @Autowired
    DictRepository dictRepository;


    public List<Map<String, Object>> getDictDataList() {
        return dictDataRepository.getDictDataList();
    }

    public List<Map<String, Object>> getDictDataListByDict_Value(@Param("dict_value") Integer dict_value) {
        return dictDataRepository.getDictDataListByDict_Value(dict_value);
    }


    //插入数据
    public DictData insertDictData(DictData dictData, Integer dictValue){

        Optional<Dict> id = dictRepository.findById(dictValue);
        Dict dict = id.isPresent() ? id.get() : null;
        if (dict ==null){
            log.error("dictValue -->{}<-- is not exist.",dictValue);
            throw new DictException(400,"dictValue is not exist.");
        }
        dict.addDictData(dictData);
        return dictDataRepository.save(dictData);
    }


    //根据id删除数据
    public void removeDictDataById(Integer id){
        Optional<DictData> dictData = dictDataRepository.findById(id);
        DictData dictdata = dictData.isPresent() ? dictData.get() : null;
        if (dictdata ==null){
            log.error("Id -->{}<-- is not exist.",id);
            throw new DictException(400,"Id is not exist.");
        }
        dictdata.getDict().removeDictData(dictdata);
        dictDataRepository.deleteById(id);
    }


}