package com.eiztracking.demo.service;

import com.eiztracking.demo.entity.PO.TCheckpoint;
import com.eiztracking.demo.entity.PO.TTracking;
import com.eiztracking.demo.entity.response.ConsignmentInfo;
import com.eiztracking.demo.entity.response.ConsignmentInfos;
import com.eiztracking.demo.entity.response.Itemscoupons;
import com.eiztracking.demo.entity.response.Trackinginfo;
import com.eiztracking.demo.repository.TrackingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@Slf4j
public class TrackingService {

    private static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
    private static SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm:ss");

    @Autowired
    TrackingRepository trackingRepository;

    @Transactional(readOnly = true)
    public ConsignmentInfos getTrackingDetail(String trackingNumber) {

        Optional<TTracking> optionalTTracking = trackingRepository.findByLabelNumber(trackingNumber);

        if (!optionalTTracking.isPresent())
            return null;
        TTracking tTracking = optionalTTracking.get();


        ConsignmentInfo consignmentInfo = new ConsignmentInfo();

        Itemscoupons itemscoupons = new Itemscoupons();


        //第一个类
        consignmentInfo.setPOD("");
        consignmentInfo.setConsignmentCode(tTracking.getLabelNumber());
        consignmentInfo.setDeadWeight("");
        consignmentInfo.setDeliveredAt("");
        consignmentInfo.setDeliveryETA("");
        consignmentInfo.setStatus("");
        consignmentInfo.setVolume("");

        //第二个类
        itemscoupons.setImageURL("");
        itemscoupons.setItem(tTracking.getLabelNumber());
        itemscoupons.setTrackinginfo(tTracking.gettCheckpoints()
                .stream()
                .sorted(Comparator.comparing(TCheckpoint::getCheckpointTime))
                .map(tCheckpoint -> {
                    Trackinginfo Trackinginfo = new Trackinginfo(tCheckpoint.getMessage(), "", formatDate.format(tCheckpoint.getCheckpointTime()), formatTime.format(tCheckpoint.getCheckpointTime()));
                    return Trackinginfo;
                }).collect(Collectors.toList()));

        List<Itemscoupons> list1 = new ArrayList<>();
        list1.add(itemscoupons);
        consignmentInfo.setItemscoupons(list1);

        List<ConsignmentInfo> list = new ArrayList<>();
        list.add(consignmentInfo);

        ConsignmentInfos cs = new ConsignmentInfos();
        cs.setConsignmentInfo(list);

        return cs;
    }

}
