package com.eiztracking.demo.service;

import com.eiztracking.demo.entity.PO.Account;
import com.eiztracking.demo.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author ludonglin
 * @creat 2020-05-07-10:59
 */
@Transactional
@Service("accountService")
public class AccountService {

    @Autowired
    AccountRepository accountRepository;


    //增加
    public Account insertAccount(Account account){
        return accountRepository.save(account);
    }
    //更新
    public Account updateAccount(Account account){
        return accountRepository.save(account);
    }
    //根据id删除
    public void deleteAccount(Integer id){
        accountRepository.deleteById(id);
    }
    //查全部
    public List<Account> getall(){
        return accountRepository.findAll();
    }
    //根据id查询一条数据
    public Account findAccountById(Integer id){
        return accountRepository.findById(id).get();
    }

    public List<Account>  findByName(String name){
        return accountRepository.findByName(name);
    }

}
