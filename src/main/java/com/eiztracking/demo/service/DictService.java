package com.eiztracking.demo.service;

import com.eiztracking.demo.entity.PO.Dict;
import com.eiztracking.demo.entity.PO.DictData;
import com.eiztracking.demo.repository.DictRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author ludonglin
 * @creat 2020-05-07-14:40
 */
@Service("dictService")
@Transactional
public class DictService {

    @Autowired
    DictRepository dictRepository;

    /**
     * 数据字典缓存map
     */
    private static HashMap<Integer, Map<Integer, String>> DICS = new HashMap<Integer, Map<Integer, String>>();

    //增加
    public Dict insertDict(Dict dict) {
        return dictRepository.save(dict);
    }

    //更新
    public Dict updateDict(Dict dict) {
        return dictRepository.save(dict);
    }

    //根据id删除
    public void deleteDict(Integer id) {
        dictRepository.deleteById(id);
    }

    //查全部
    public List<Map<String, Object>> getDictDataList() {
        return dictRepository.getDictList();
    }

    //根据id查询一条数据
    public List<Map<String, Object>> getDictByDictValue(Integer dictvalue) {
        return dictRepository.getDictByDictValue(dictvalue);
    }


    @PostConstruct
    public void load() {

        List<Dict> dicts = dictRepository.findAll();

        dicts.stream().forEach(tDict -> {
            Map<Integer, String> dictDataMap = tDict.getDictdata().stream()
                    .collect(Collectors.toMap(DictData::getDictDataValue, DictData::getDictDataName));

            DICS.put(tDict.getDictValue(), dictDataMap);
        });
    }

    public static Map<Integer, String> getDicsByType(int typeCode) {
        Map<Integer, String> dictDataMap = DICS.get(typeCode);
        return dictDataMap;
    }


}
