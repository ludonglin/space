package com.eiztracking.demo.job.couriersplease;

import lombok.Data;

@Data
public class Errors {

    private String code;
    private String name;
    private String message;
}
