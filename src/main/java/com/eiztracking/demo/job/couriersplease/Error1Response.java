package com.eiztracking.demo.job.couriersplease;

import lombok.Data;

import java.util.List;

@Data
public class Error1Response {

    private List<Errors> errors;
}
