package com.eiztracking.demo.job;

import com.alibaba.fastjson.JSONObject;
import com.eiztracking.demo.common.DateTimeUtils;
import com.eiztracking.demo.common.HttpClientPoolUtil;
import com.eiztracking.demo.common.MD5Util;
import com.eiztracking.demo.constant.CourierConstant;
import com.eiztracking.demo.constant.TrackingConstant;
import com.eiztracking.demo.entity.PO.TCheckpoint;
import com.eiztracking.demo.entity.PO.TTracking;
import com.eiztracking.demo.entity.response.BaseResponse;
import com.eiztracking.demo.entity.response.ConsignmentInfos;
import com.eiztracking.demo.entity.response.Trackinginfo;
import com.eiztracking.demo.exception.CourierException;
import com.eiztracking.demo.job.couriersplease.Error1Response;
import com.eiztracking.demo.repository.TrackingRepository;
import com.eiztracking.demo.service.DictService;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author ludonglin
 * @creat 2020-05-19-9:32
 */

@Component
@Slf4j


public class CouriersPleaseTrackingJob {

    @Autowired
    Gson gson;

    @Autowired
    TrackingRepository trackingRepository;

    @Autowired
    private DictService dictService;


    private static SimpleDateFormat sdf = new SimpleDateFormat("d MMM yyyy K:m a", Locale.ENGLISH);

    @Scheduled(fixedDelay = 65000, initialDelay = 5000)
    @Transactional(rollbackFor = Exception.class)
    public void track() throws ExecutionException, InterruptedException {

        log.debug("CouriersPlease tracking job begin at " + DateTimeUtils.getNow());

        Optional<List<TTracking>> optionalTTrackings = trackingRepository.findTop15ByServiceProviderEqualsAndStatusEqualsAndNextTrackingTimeLessThanEqualOrderByNextTrackingTimeAsc(CourierConstant.COURIER_TYPE_COURIERSPLEASE,
                TrackingConstant.TRACKING_JOB_STATUS_TRACKING, System.currentTimeMillis() / 1000);

        if (!optionalTTrackings.isPresent()) {
            return;
        }
        List<TTracking> tTrackings = optionalTTrackings.get();

        if ("".equals(tTrackings)) {
            return;
        }

        List<String> trackingNumberList = tTrackings.stream().map(TTracking::getLabelNumber).collect(Collectors.toList());

        for (String trackingNum : trackingNumberList) {
            TTracking tTracking = tTrackings.stream().filter(t -> t.getLabelNumber().equals(trackingNum)).findFirst().get();

            ConsignmentInfos infos = createTrackingFuture(trackingNum);
            //判断response不正常情况
            if (infos == null) {
                updateTrackingNextTime(tTrackings, trackingNum);
                continue;
            }
            //判断空
            if (infos.getConsignmentInfo().get(0).getItemscoupons().get(0).getTrackinginfo().get(0).getAction() == null || infos.getConsignmentInfo().get(0).getItemscoupons().get(0).getTrackinginfo().get(0).getAction().equals("No scan events found")) {
                updateTrackingNextTime(tTrackings, trackingNum);
                continue;
            }

            int trackinginfoSize = infos.getConsignmentInfo().get(0).getItemscoupons().get(0).getTrackinginfo().size();

            Integer resultsNumber = tTracking.getTrackingResultsNumber();

            //如果大于resultNum，说明要去插入新数据
            if (trackinginfoSize > resultsNumber.intValue()) {

                //正序插入
                for (int i = resultsNumber.intValue(); i <= trackinginfoSize - 1; i++) {
                    Trackinginfo trackinginfo = infos.getConsignmentInfo().get(0).getItemscoupons().get(0).getTrackinginfo().get(i);

                    TCheckpoint tCheckpoint = new TCheckpoint();

                    String message = trackinginfo.getAction();

                    String yearMonthDay = trackinginfo.getDate();
                    String time = trackinginfo.getTime();
                    String datetime = yearMonthDay + " " + time;
                    ParsePosition pos = new ParsePosition(0);
                    Date checkpointTime = sdf.parse(datetime, pos);

                    tCheckpoint.settTracking(tTracking);
                    tCheckpoint.setMessage(message);
                    tCheckpoint.setCheckpointTime(checkpointTime);
                    tCheckpoint.setCreatedAt(DateTimeUtils.getNow());


                    if (message.contains("Picked Up") || message.contains("accepted") || message.contains("Accepted")) {
                        tCheckpoint.setTrackingStatus(TrackingConstant.TRACKING_STATUS_INFORECEIVED);
                    } else if (message.contains("delivery")) {
                        tCheckpoint.setTrackingStatus(TrackingConstant.TRACKING_STATUS_OUTFORDELIVERY);
                    } else if (message.contains("delivered") || message.contains("Redirected")) {
                        tCheckpoint.setTrackingStatus(TrackingConstant.TRACKING_STATUS_DELIVERED);
                    } else if (message.contains("in transit") || message.contains("Attempted") || message.contains("sent") || message.contains("arrived")) {
                        tCheckpoint.setTrackingStatus(TrackingConstant.TRACKING_STATUS_INTRANSIT);
                    }

                    if (i == trackinginfoSize - 1) {
                        tTracking.setLastCheckpointTime(checkpointTime);
                        tTracking.setTrackingStatus(tCheckpoint.getTrackingStatus());
                        tTracking.setStatusString(dictService.getDicsByType(TrackingConstant.TRACKING_STATUS).get(tTracking.getTrackingStatus()));
                    }

                    tTracking.addTCheckpoint(tCheckpoint);
                }

            }

            tTracking.setTrackingResultsNumber(trackinginfoSize);
            tTracking.setNextTrackingTime(DateTimeUtils.getNextTwoHourTimeMillis());
            tTracking.setUpdatedAt(DateTimeUtils.getNow());

        }
        trackingRepository.saveAll(tTrackings);


    }

    private void updateTrackingNextTime(List<TTracking> tTrackings, String trackingNumber) {
        TTracking tTracking = tTrackings.stream().filter(t -> t.getLabelNumber().equals(trackingNumber)).findFirst().get();
        tTracking.setNextTrackingTime(DateTimeUtils.getNextTwoHourTimeMillis());
        tTracking.setUpdatedAt(DateTimeUtils.getNow());
    }


    public ConsignmentInfos createTrackingFuture(String trackingNumber) {

        Map<String, String> httpHeaderMap = new HashMap<>();
        httpHeaderMap.put("Host", "api.couriersplease.com.au");
        httpHeaderMap.put("Accept", "application/json");
        httpHeaderMap.put("Authorization", "Basic " + MD5Util.MD5Encode("113122022:631BDA1D0286265495567F103A5E8298F15F61BFA9DD240D4998792FAF06561D"));


        String url = "https://api.couriersplease.com.au/v1/domestic/locateParcel?trackingCode=" + trackingNumber;

        ConsignmentInfos consignmentInfos = null;
        String responseJson = null;

        try {

            responseJson = HttpClientPoolUtil.get(url, httpHeaderMap);
            BaseResponse baseResponse = gson.fromJson(responseJson, BaseResponse.class);
            consignmentInfos = JSONObject.parseObject(JSONObject.toJSONString(baseResponse.getData()), ConsignmentInfos.class);

            //BaseResponse baseResponse = JSONObject.parseObject(responseJson, BaseResponse.class);
            //consignmentInfos = JSONObject.parseObject(JSONObject.toJSONString(baseResponse.getData()), ConsignmentInfos.class);
            if (consignmentInfos!=null){
                String item = consignmentInfos.getConsignmentInfo().get(0).getItemscoupons().get(0).getItem();

                if ("".equals(item)) {
                    consignmentInfos.getConsignmentInfo().get(0).getItemscoupons().get(0).setItem(trackingNumber);
                }
            }

            log.debug(responseJson);
        } catch (CourierException e) {
            log.error(e.getMessage());
            if (consignmentInfos == null)
                throw new CourierException("CourisrsPlease tracking failed, responses null");
        } finally {

        }

        return consignmentInfos;

    }



}


